$(function(){
	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
	ua = navigator.userAgent,

	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

	scaleFix = function () {
		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
			document.addEventListener("gesturestart", gestureStart, false);
		}
	};
	
	scaleFix();
});
var ua=navigator.userAgent.toLocaleLowerCase(),
 regV = /ipod|ipad|iphone/gi,
 result = ua.match(regV),
 userScale="";
if(!result){
 userScale=",user-scalable=0"
}
document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0'+userScale+'">')

// ЕСЛИ ПРОЕКТ РЕСПОНСИВ ТО ВСЕ ЧТО ВЫШЕ НУЖНО РАССКОМЕНТИРОВАТЬ. СКРИПТ ВЫШЕ ПРЕДНАЗНАЧЕН ДЛЯ КОРРЕКТНОГО ОТОБРАЖЕНИЯ ВЕРСТКИ ПРИ СМЕНЕ ОРИЕНТАЦИИ НА ДЕВАЙСАХ



// INCLUDE FUNCTION
// ниже пример подключения табов. То есть создаем переменную с селектором на который нужно выполнить инициализацию, потом условием проверяем есть ли такой селектор, если есть то скрипт подключит файл с помощью функции INCLUDE. Это важно так как в наших проектах может быть подключено по 20 библиотек это оочеь плохо так как проект становится тяжелым и переполненным мусором

var sticky = $(".sticky"),
	maxheight = $(".maxheight"),
	wow = $(".wow"),
    styler = $(".styler"),
	windowW = $(window).width();

if(sticky.length){
  include("js/sticky.js");
  include("js/jquery.smoothscroll.js");
}

if(maxheight.length){
  include("js/jquery.matchHeight-min.js");
}

if(wow.length){
	include("js/wow.min.js");
}

if(styler.length){
  // include("js/jquery.formstyler.js");
  include("js/jquery.formstyler.min.js");
}

	include("js/modernizr.js");




// Плагин скриптом задает высоту блоку и выставляет по самому большому , нужно чтобы на блоке висел класс (data-mh) и атрибут (data-mh) с одним названием для одинаковых блоков, тогда он будет работать, выше находиться его инициализация, еще он хорошо работает при респонзиве. 

function include(url){ 
  document.write('<script src="'+ url + '"></script>'); 
}




$(document).ready(function(){

	// =============================sticky start=======================================

		if (sticky.length){
			$(sticky).sticky({
		        topspacing: 0,
		        styler: 'is-sticky',
		        animduration: 0,
		        unlockwidth: false,
		        screenlimit: false,
		        sticktype: 'alonemenu'
			});
		};

	// =============================sticky end=======================================


	// =======================================anchor start=======================================
		 
	    function goUp(){
			var windowHeight = $(window).height(),
				windowScroll = $(window).scrollTop();

			if(windowScroll>windowHeight/2){
			  $('.arrow_up').addClass('active');
			}

			else{
			  $('.arrow_up').removeClass('active');
			  $('.arrow_up').removeClass('click');
			}

	    }

	    goUp();
		$(window).on('scroll',goUp);

		$('.arrow_up').on('click ontouchstart',function () {

			if($.browser.safari){
				$(this).addClass("click")
				$('body').animate( { scrollTop: 0 }, 1100 );
			}
			else{
				$(this).addClass("click")
				$('html,body').animate( { scrollTop: 0}, 1100 );
			}
			return false;
			
		});

	// =======================================anchor end=======================================


	// =====================================Animate block Start=====================================

		if(wow.length){
	        if($("html").hasClass("md_no-touch")){
				new WOW().init();	
			}
			else if($("html").hasClass("md_touch")){
				$("body").find(".wow").css("visibility","visible");
			}

		}

	// =====================================Animate block End  =====================================


	// =====================================Responsive menu Start=====================================

			$(".resp_btn, .close_resp_menu").on("click ontouchstart", function(){
				$("body").toggleClass("show_menu")
			});

			$(document).on("click ontouchstart", function(event) {
		      if ($(event.target).closest("nav,.resp_btn").length) return;
		      $("body").removeClass("show_menu");
		      if(windowW <= 991){
		      	$(".menu_item").removeClass("active").find(".dropdown-menu").css("display","none");
		      }
		      event.stopPropagation();
		    });

			// проверка на наличие элемента и вставка хтмл кода
		 	//  if($(window).width() <= 767){
			// 	if ($(".menu_item").length){
			//         $(".menu_item").has('.dropdown-menu').append('<span class="touch-button"><i class="fa fa-angle-down"></i></span>');
			//     }
			// }
			// проверка на наличие элемента и вставка хтмл кода


			$('.menu_link').on('click ontouchstart',function(event){
				if($("html").hasClass("md_no-touch"))return;

		        var windowWidth = $(window).width(),
		            $parent = $(this).parent('.menu_item');
		        if(windowWidth > 991){
		          // if($("html").hasClass("md_touch")){
		            if((!$parent.hasClass('active')) && $parent.find('.dropdown-menu').length){

		              event.preventDefault();

		              $parent.toggleClass('active')
		               .siblings()
		               .find('.menu_link')
		               .removeClass('active');
		            }
		          // }  
		        }
		        
		        else{
		            
		          if((!$parent.hasClass('active')) && $parent.find('.dropdown-menu').length){

		            event.preventDefault();

		            $parent.toggleClass('active')
		             .siblings()
		             .removeClass('active');
		            $parent.find(".dropdown-menu")
		             .slideToggle()
		             .parents('.menu_item')
		             .siblings()
		             .find(".dropdown-menu")
		             .slideUp();
		          }
		        }

		    });

	// =====================================Responsive menu End  =====================================


	// =====================================formstyler start=====================================

		if (styler.length){
			styler.styler({
				selectSmartPositioning: true
			});
		}

	// =====================================formstyler end=====================================
})